﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace cobaProyekDatmin
{
    public partial class ProcessForm : Form
    {
        int targetAttribute = -1;
        List<HasilIf> hasilIfList = new List<HasilIf>();
        IEnumerable<PenampungUnique> uniques;
        List<ComboBox> testingComboBoxList = new List<ComboBox>();

        public class PenampungUnique
        {
            public List<String> Field;
            public int Count;

            private string _toString = null;
            private int _hashCode = -1;
            
            public void hapusField(int indeks)
            {
                this.Field.RemoveAt(indeks);
            }
            
            public int _GetHashCode()
            {
                return ToString().GetHashCode();
            }
            
            public string _ToString()
            {
                string forReturn = Count.ToString() + ",";
                foreach(string coba in Field)
                {
                    forReturn = forReturn + coba + ",";
                }
                return forReturn;
            }

            public override int GetHashCode()
            {
                if (_hashCode == -1)
                    _hashCode = _GetHashCode();
                return _hashCode;
            }

            public override string ToString()
            {
                if (_toString == null)
                    _toString = _ToString();
                return _toString;
            }
        }

        public class PenampungUniqueComparator:IEqualityComparer<PenampungUnique>
        {
            public bool Equals(PenampungUnique x, PenampungUnique y)
            {
                return x.ToString() == y.ToString();
            }

            public int GetHashCode(PenampungUnique obj)
            {
                return obj.GetHashCode();
            }
        }

        [DebuggerDisplay("{ToString()}")]
        public class HasilIf : Stack<HasilWhere> {
            public string target;

            public override string ToString() {
                var daftarWhere = from hasilWhere in this select hasilWhere.ToString();
                daftarWhere = daftarWhere.Reverse();
                return "If " + string.Join(Environment.NewLine + "   and ", daftarWhere) + " then " + target;
            }

            public string Predict(string[] inputan) {
                foreach (HasilWhere hasilWhere in this) {
                    if (inputan[hasilWhere.Attribut] != hasilWhere.Value)
                        return null;
                }
                return target;
            }
        }

        public class HasilWhere
        {
            public int Attribut;
            public string Value;

            public override string ToString()
            {
                return MainForm.headers[Attribut].nama + " = " + Value;
            }
        }

        public class HasilBenarSalah
        {
            public int Attribut;
            public string Value;
            public int Benar = 0;
            public int Total = 0;
            
            public override string ToString()
            {
                return MainForm.headers[Attribut].nama + " = " + Value + " " + (Benar / (float) Total).ToString();
            }
        }

        private void btnProcess_Click(object sender, EventArgs e)
        {
            if(targetAttribute <= -1) {
                MessageBox.Show("Please Select Target Attribute");
                return;
            }
            testGroupBox.Visible = false;
            resultTxtBox.Text = "";
            hasilIfList.Clear();
            var tampungUniqueComparator = new PenampungUniqueComparator();
            progressBar1.Maximum = uniques.Count();
            progressBar1.Value = 0;
            var daftarTargetUnik = uniques.Select(x => x.Field[targetAttribute]).Distinct();
            foreach (var target in daftarTargetUnik) {
                var tampung = uniques.ToList();
                logGroupBox.Text = "Target Class = " + target;
                Update();
                //infinite loop sampai habis tampungnya
                while (true) {
                    MainForm.clearBlacklistHeader();
                    MainForm.blacklistHeader(targetAttribute);
                    var a = HitungPrism(tampung, target);
                    //kalau data sudah habis, lanjut ke target berikutnya
                    if (a == null)
                        break;
                    //jika belum, dapetin data selain yang dipilih untuk rule IF tadi
                    tampung = tampung.Except(a, tampungUniqueComparator).ToList();
                    progressBar1.Increment(a.Count());
                    Update();
                }
            }
            int benar = 0;
            foreach (string[] dataRow in MainForm.dataRows) {
                int a = 0;
                foreach (HasilIf hasilIf in hasilIfList) {
                    a++;
                    string hasil = hasilIf.Predict(dataRow);
                    if (hasil == null)
                        continue;
                    if (hasil == dataRow[targetAttribute])
                        benar++;
                    break;
                }
            }
            //resultTxtBox.Text = benar.ToString() + " - " + MainForm.dataRows.Count().ToString() + Environment.NewLine;
            resultTxtBox.Text = "";
            string tampungOutput = "";
            foreach (HasilIf hasilIf in hasilIfList) {
                tampungOutput += hasilIf.ToString() + Environment.NewLine;
            }
            resultTxtBox.Text = tampungOutput;
            testGroupBox.Visible = true;
        }

        private void comboAttr_SelectedIndexChanged(object sender, EventArgs e)
        {
            targetAttribute = comboAttr.SelectedIndex;
        }
        
        public ProcessForm()
        {
            InitializeComponent();
            foreach(MainForm.Header header in MainForm.headers) {
                comboAttr.Items.Add(header.nama);
            }
            uniques = MainForm.dataRows
                .GroupBy(row => {
                    //concat semua data dari satu baris jadi satu string
                    string ret = "";
                    foreach (string kolom in row)
                    {
                        ret += kolom + ",";
                    }
                    return ret;
                }).Select(x => new PenampungUnique { Field = x.First().ToList(), Count = x.Count() })
                .ToList();
            CreateTestControls();
        }

        private void CreateTestControls() {
            foreach (Control item in testGroupBox.Controls) {
                testGroupBox.Controls.Remove(item);
            }
            int iHead = 0;
            foreach (MainForm.Header header in MainForm.headers) {
                Label label = new Label();
                label.Text = header.nama;
                label.Left = 6;
                label.AutoSize = true;
                label.Top = 20 + iHead * 50;
                label.Parent = testGroupBox;
                ComboBox combo = new ComboBox();
                string[] values = uniques.Select(x => x.Field[iHead]).Distinct().ToArray();
                combo.Items.AddRange(values);
                combo.Left = 8;
                combo.Top = 43 + iHead * 50;
                combo.Width = testGroupBox.Width - 16;
                combo.Parent = testGroupBox;
                testingComboBoxList.Add(combo);
                iHead++;
            }
            Button testButton = new Button();
            testButton.Text = "Classify";
            testButton.Width = testGroupBox.Width - 14;
            testButton.Height = 30;
            testButton.Left = 7;
            testButton.Top = 30 + iHead * 50;
            testButton.UseVisualStyleBackColor = true;
            testButton.Parent = testGroupBox;
            testButton.Click += TestButton_Click;
        }

        private void TestButton_Click(object sender, EventArgs e) {
            string[] dataRow = new string[testingComboBoxList.Count()];
            for (int i = testingComboBoxList.Count() - 1; i >= 0; i--) {
                dataRow[i] = testingComboBoxList[i].Text;
            }
            foreach (HasilIf hasilIf in hasilIfList) {
                string hasil = hasilIf.Predict(dataRow);
                if (hasil == null)
                    continue;
                MessageBox.Show(hasil);
                return;
            }
            MessageBox.Show("Uh oh, prediksi tidak ditemukan!");
        }

        /// <summary>
        /// Menghasilkan hasil data untuk satu rule IF
        /// </summary>
        /// <param name="data"></param>
        /// <param name="targetValue"></param>
        /// <param name="whereList"></param>
        /// <returns></returns>
        public IEnumerable<PenampungUnique> HitungPrism(IEnumerable<PenampungUnique> data, string targetValue, HasilIf whereList = null)
        {
            //jika data kosong, langsung the end.
            if (data.Count() == 0)
                return null;

            //inisialisasi whereList jika belum ada
            if (whereList == null)
                whereList = new HasilIf { target = targetValue };
            
            List<HasilBenarSalah> hasilBenarSalahList = new List<HasilBenarSalah>();
            for (int i = 0; i < MainForm.headers.Length; i++)
            {
                if (MainForm.headers[i].blacklist) //jika di blacklist, skip!
                    continue;
                //mendapatkan daftar nilai unik untuk kolom ini
                var daftarNilaiUnik = data.Select(x => x.Field[i]).Distinct();
                foreach (string nilaiUnik in daftarNilaiUnik) {
                    //untuk setiap unik, hitung jumlah benar nya
                    IEnumerable<PenampungUnique> dataUntukNilaiIni = data.Where(x => x.Field[i] == nilaiUnik);
                    int total = dataUntukNilaiIni.Count();
                    int benar = dataUntukNilaiIni.Where(x => x.Field[targetAttribute] == targetValue).Count();
                    HasilBenarSalah hasilBenarSalah = new HasilBenarSalah
                    {
                        Attribut = i,
                        Value = nilaiUnik,
                        Benar = benar,
                        Total = total
                    };
                    hasilBenarSalahList.Add(hasilBenarSalah);
                }
            }

            //sorting dan ambil yang paling bagus benar salah nya
            hasilBenarSalahList = hasilBenarSalahList
                .OrderByDescending(x => x.Benar / (float)x.Total)
                .ThenByDescending(x => x.Total)
                .ToList();
            HasilBenarSalah hbs = hasilBenarSalahList.First();
            if (SpeedBar.Value > 0) {
                LogBox.Text = string.Join(" ", hasilBenarSalahList.Select(x => x + " " + x.Benar + "/" + x.Total + "\r\n"));
                resultTxtBox.Text += LogBox.Lines.FirstOrDefault() + "\r\n";
                Update();
                System.Threading.Thread.Sleep(SpeedBar.Value / 2);
            }

            if (hbs.Benar == 0)
                return null; //anggap data sudah habis karena sudah tidak ada yang benar

            whereList.Push(new HasilWhere
            {
                Attribut = hbs.Attribut,
                Value = hbs.Value
            });

            data = data.Where(x => x.Field[hbs.Attribut] == hbs.Value);
            if (hbs.Total == hbs.Benar) {
                //sudah 100% benar, tidak perlu cari sampe dalam lagi
                hasilIfList.Add(whereList);
                if (SpeedBar.Value > 0) {
                    resultTxtBox.Text = String.Join(Environment.NewLine, whereList + "");
                    Update();
                    resultTxtBox.Text = "";
                    System.Threading.Thread.Sleep(SpeedBar.Value);
                }
                return data;
            }

            MainForm.blacklistHeader(hbs.Attribut);
            if (MainForm.headers.Count() == MainForm.headerBlacklistCount) {
                //jika semua header sudah di blacklist, berarti done
                hasilIfList.Add(whereList);
                if (SpeedBar.Value > 0) {
                    resultTxtBox.Text = String.Join(Environment.NewLine, whereList + "");
                    Update();
                    resultTxtBox.Text = "";
                    System.Threading.Thread.Sleep(SpeedBar.Value);
                }
                return data;
            }

            //jika masih belum, lanjut masuk dalem
            return HitungPrism(data, targetValue, whereList);
        }

        /*public List<HasilBenarSalah> cariTerbesar(IEnumerable<PenampungUnique> data,List<HasilBenarSalah> listhbs, int counterRecursive, bool selesai)
        {
            if(selesai == true)
            {
                //MessageBox.Show("if sudah dicheck persenan terbesar sampai ke field terakhir");
                counter = 0;
                ctrSelesai = 0;
                bandingTerbesar = 0;
                tampungTotal = 0;
                tampungBenar = 0;
                return untukDiReturn;
            }
            else
            {
                if(counterRecursive >= listhbs.Count())
                {
                    listhbs = hapusDataRow(data, untukDiReturn);
                    return cariTerbesar(data,listhbs,counter,true);
                }
                else
                {
                    /*int benarNya = listhbs[counterRecursive].Benar;
                    int totalNya = listhbs[counterRecursive].Total;
                    double tempTerbesar = ((Convert.ToDouble(totalNya) - Convert.ToDouble(benarNya)) / Convert.ToDouble(totalNya)) * 100;
                    if (tempTerbesar == bandingTerbesar) //jika persenannya sama, maka diambil dari selisih terkecil
                    {
                        int cekKembarNow = totalNya - benarNya;
                        int cekKembarOld = tampungTotal - tampungBenar;
                        if (cekKembarNow == cekKembarOld)
                        {
                            ctrSelesai++;
                        }
                        else if (cekKembarNow < cekKembarOld)
                        {
                            tampungBenar = benarNya;
                            tampungTotal = totalNya;
                            bandingTerbesar = tempTerbesar;
                            untukDiReturn.Add(listhbs[counterRecursive]);
                        }
                    }
                    else if (tempTerbesar > bandingTerbesar) //jika persenan lebih besar, akan langsung diambil
                    {
                        //MessageBox.Show("if diatas");
                        tampungBenar = benarNya;
                        tampungTotal = totalNya;
                        bandingTerbesar = tempTerbesar;
                        untukDiReturn.Add(listhbs[counterRecursive]);

                    }
                    if (ctrSelesai == listhbs.Count() - 1)
                    {
                        return cariTerbesar(data, listhbs, counter, true);
                    }
                    else
                    {
                        counter += 1;
                        ctrSelesai = 0;
                        var tampung = uniques.ToList();
                        int index = 0;
                        for(int i = 0; i < MainForm.headers.Length; i++)
                        {
                            if(listhbs[counterRecursive].Attribut == MainForm.headers[i].nama)
                            {
                                index = i;
                                break;
                            }
                        }
                        foreach (var item in data)
                        {
                            item.hapusField(index);
                        }
                        return cariTerbesar(data, listhbs, counter, false);
                    }*
                }
                     
            }
        }*/

        /*public List<HasilBenarSalah> hapusDataRow(IEnumerable<PenampungUnique> data, List<HasilBenarSalah> hasilBenarSalahList)
        {
            string[] deleteAttr = new string[hasilBenarSalahList.Count()];
            string[] deleteVal = new string[hasilBenarSalahList.Count()];
            //List<int> kurangiIndex = new List<int>();
            for (int i = 0; i < hasilBenarSalahList.Count(); i++)
            {
                //kurangiIndex.Add(hasilBenarSalahList.Count() - hasilBenarSalahList.Count() - i);
                //ini ngambil data 1 1, smoga jalan
                data = this.data.Where(x => x.Field[i] == hasilBenarSalahList[i].Value);
            }
            int uh = 0;
            //IEnumerable<PenampungUnique> temp = uniques.Where(x => x.Field[fieldIndex] != deletedVal).ToList();
            //data = temp;
            //MessageBox.Show(uniques.Count().ToString());

            hasilBenarSalahList.Clear();
            for (int i = 0; i < MainForm.headers.Length; i++)
            {
                if (i == targetAttribute) //jika target, skip!
                    continue;
                string[] daftarNilaiUnik = data.Select(x => x.Field[i]).Distinct().ToArray();
                foreach (string nilaiUnik in daftarNilaiUnik)
                {
                    IEnumerable<PenampungUnique> dataUntukNilaiIni = this.data.Where(x => x.Field[i] == nilaiUnik);
                    int total = dataUntukNilaiIni.Count();
                    int benar = dataUntukNilaiIni.Where(x => x.Field[targetAttribute] == targetValue).Count();
                    HasilBenarSalah hasilBenarSalah = new HasilBenarSalah
                    {
                        Attribut = i,
                        Value = nilaiUnik,
                        Benar = benar,
                        Total = total
                    };
                    hasilBenarSalahList.Add(hasilBenarSalah);
                }
            }
            return hasilBenarSalahList;
        }*/
    }
}
