﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace cobaProyekDatmin
{
    public partial class GraphicsForm : Form
    {
        public GraphicsForm()
        {
            InitializeComponent();
            foreach (MainForm.Header header in MainForm.headers)
            {
                comboBox1.Items.Add(header.nama);
            }
            dataGridView1.ColumnCount = MainForm.headers.Count();
           
            dataGridView1.RowCount = MainForm.dataRows.Count();
            for (int i = 0; i < MainForm.headers.Count(); i++)
            {
                dataGridView1.Columns[i].HeaderText = MainForm.headers[i].nama;
            }
            for (int i = 0; i < MainForm.dataRows.Count(); i++)
            {
                for (int j = 0; j<MainForm.headers.Count(); j++)
                {
                    dataGridView1.Rows[i].Cells[j].Value = MainForm.dataRows[i][j].ToString();
                    dataGridView1.Rows[i].HeaderCell.Value = (i + 1).ToString();
                }
            }
            comboBox1.SelectedIndex = 0;
        }

        private void dataGridView1_ColumnAdded(object sender, DataGridViewColumnEventArgs e)
        {
            e.Column.FillWeight = MainForm.dataRows.Count();
        }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            int idx = comboBox1.SelectedIndex;
            chart1.Series.Clear();
            List<string> asd = new List<string>();
            asd.Clear();
            for(int i = 0; i < MainForm.dataRows.Count(); i++)
            {
                asd.Add(MainForm.dataRows[i][idx]);
            }
            var daftarNilaiUnik = asd.Distinct();    
            foreach (string nilaiUnik in daftarNilaiUnik)
            {
                int total = asd.Where(x => x == nilaiUnik).Count();
                chart1.Series.Add(nilaiUnik);
                chart1.Series[nilaiUnik].Points.AddY(total);
            }

        }
    }
}
