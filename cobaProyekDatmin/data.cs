﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace cobaProyekDatmin
{
    class data
    {
        
        private string gender, race, parentalEducation, lunch, testPreparation, mathScore, readingScore, writingScore;

        public data(string gender, string race, string parentalEducation, string lunch, string testPreparation, string mathScore,
            string readingScore, string writingScore)
        {
            this.gender = gender;
            this.race = race;
            this.parentalEducation = parentalEducation;
            this.lunch = lunch;
            this.testPreparation = testPreparation;
            this.mathScore = mathScore;
            this.readingScore = readingScore;
            this.writingScore = writingScore;
        }

        private void setGender(string gender)
        {
            this.gender = gender;
        }

        public string getGender()
        {
            return this.gender;
        }

        private void setRace(string race)
        {
            this.race = race;
        }

        public string getRace()
        {
            return this.race;
        }

        private void setParentalEducation(string parentalEducation)
        {
            this.parentalEducation = parentalEducation;
        }

        public string getParentalEducation()
        {
            return this.parentalEducation;
        }

        private void setLunch(string lunch)
        {
            this.lunch = lunch;
        }

        public string getLunch()
        {
            return this.lunch;
        }

        private void setTestPreparation(string testPreparation)
        {
            this.testPreparation = testPreparation;
        }

        public string getTestPreparation()
        {
            return this.testPreparation;
        }

        private void setMathScore(string mathScore)
        {
            this.mathScore = mathScore;
        }

        public string getMathScore()
        {
            return this.mathScore;
        }

        private void setReadingScore(string readingScore)
        {
            this.readingScore = readingScore;
        }

        public string getReadingScore()
        {
            return this.readingScore;
        }
        private void setWritingScore(string writingScore)
        {
            this.writingScore = writingScore;
        }

        public string getWritingScore()
        {
            return this.writingScore;
        }
    }
}
