﻿namespace cobaProyekDatmin
{
    partial class ProcessForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnProcess = new System.Windows.Forms.Button();
            this.comboAttr = new System.Windows.Forms.ComboBox();
            this.attrlabel = new System.Windows.Forms.Label();
            this.resultTxtBox = new System.Windows.Forms.TextBox();
            this.logGroupBox = new System.Windows.Forms.GroupBox();
            this.LogBox = new System.Windows.Forms.RichTextBox();
            this.progressBar1 = new System.Windows.Forms.ProgressBar();
            this.label1 = new System.Windows.Forms.Label();
            this.SpeedBar = new System.Windows.Forms.TrackBar();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.testGroupBox = new System.Windows.Forms.GroupBox();
            this.logGroupBox.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.SpeedBar)).BeginInit();
            this.SuspendLayout();
            // 
            // btnProcess
            // 
            this.btnProcess.Location = new System.Drawing.Point(13, 118);
            this.btnProcess.Name = "btnProcess";
            this.btnProcess.Size = new System.Drawing.Size(461, 27);
            this.btnProcess.TabIndex = 2;
            this.btnProcess.Text = "Process!";
            this.btnProcess.UseVisualStyleBackColor = true;
            this.btnProcess.Click += new System.EventHandler(this.btnProcess_Click);
            // 
            // comboAttr
            // 
            this.comboAttr.BackColor = System.Drawing.SystemColors.Window;
            this.comboAttr.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboAttr.FormattingEnabled = true;
            this.comboAttr.Location = new System.Drawing.Point(12, 21);
            this.comboAttr.Name = "comboAttr";
            this.comboAttr.Size = new System.Drawing.Size(462, 25);
            this.comboAttr.TabIndex = 2;
            this.comboAttr.SelectedIndexChanged += new System.EventHandler(this.comboAttr_SelectedIndexChanged);
            // 
            // attrlabel
            // 
            this.attrlabel.AutoSize = true;
            this.attrlabel.Location = new System.Drawing.Point(186, -1);
            this.attrlabel.Name = "attrlabel";
            this.attrlabel.Size = new System.Drawing.Size(105, 19);
            this.attrlabel.TabIndex = 16;
            this.attrlabel.Text = "Target Attribute";
            // 
            // resultTxtBox
            // 
            this.resultTxtBox.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.resultTxtBox.Location = new System.Drawing.Point(483, 21);
            this.resultTxtBox.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.resultTxtBox.MaxLength = 65535;
            this.resultTxtBox.Multiline = true;
            this.resultTxtBox.Name = "resultTxtBox";
            this.resultTxtBox.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this.resultTxtBox.Size = new System.Drawing.Size(444, 575);
            this.resultTxtBox.TabIndex = 1;
            this.resultTxtBox.WordWrap = false;
            // 
            // logGroupBox
            // 
            this.logGroupBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.logGroupBox.Controls.Add(this.LogBox);
            this.logGroupBox.Location = new System.Drawing.Point(13, 170);
            this.logGroupBox.Name = "logGroupBox";
            this.logGroupBox.Size = new System.Drawing.Size(464, 426);
            this.logGroupBox.TabIndex = 17;
            this.logGroupBox.TabStop = false;
            this.logGroupBox.Text = "Target Class = ";
            // 
            // LogBox
            // 
            this.LogBox.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.LogBox.Location = new System.Drawing.Point(6, 24);
            this.LogBox.Name = "LogBox";
            this.LogBox.Size = new System.Drawing.Size(452, 396);
            this.LogBox.TabIndex = 0;
            this.LogBox.Text = "";
            // 
            // progressBar1
            // 
            this.progressBar1.Location = new System.Drawing.Point(12, 154);
            this.progressBar1.Margin = new System.Windows.Forms.Padding(4, 3, 5, 3);
            this.progressBar1.Name = "progressBar1";
            this.progressBar1.Size = new System.Drawing.Size(462, 10);
            this.progressBar1.TabIndex = 18;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(186, 49);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(85, 19);
            this.label1.TabIndex = 19;
            this.label1.Text = "Select Speed";
            // 
            // SpeedBar
            // 
            this.SpeedBar.Location = new System.Drawing.Point(67, 71);
            this.SpeedBar.Maximum = 2000;
            this.SpeedBar.Name = "SpeedBar";
            this.SpeedBar.Size = new System.Drawing.Size(352, 45);
            this.SpeedBar.TabIndex = 20;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(425, 71);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(49, 19);
            this.label2.TabIndex = 21;
            this.label2.Text = "Slower";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(15, 71);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(46, 19);
            this.label3.TabIndex = 22;
            this.label3.Text = "Faster";
            // 
            // testGroupBox
            // 
            this.testGroupBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.testGroupBox.Location = new System.Drawing.Point(12, 170);
            this.testGroupBox.Name = "testGroupBox";
            this.testGroupBox.Size = new System.Drawing.Size(465, 426);
            this.testGroupBox.TabIndex = 23;
            this.testGroupBox.TabStop = false;
            this.testGroupBox.Text = "Classification Test";
            this.testGroupBox.Visible = false;
            // 
            // ProcessForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 17F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(940, 608);
            this.Controls.Add(this.testGroupBox);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.SpeedBar);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.progressBar1);
            this.Controls.Add(this.logGroupBox);
            this.Controls.Add(this.btnProcess);
            this.Controls.Add(this.resultTxtBox);
            this.Controls.Add(this.comboAttr);
            this.Controls.Add(this.attrlabel);
            this.Font = new System.Drawing.Font("Segoe UI", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.Name = "ProcessForm";
            this.Text = "Process";
            this.logGroupBox.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.SpeedBar)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Label attrlabel;
        private System.Windows.Forms.Button btnProcess;
        private System.Windows.Forms.ComboBox comboAttr;
        private System.Windows.Forms.TextBox resultTxtBox;
        private System.Windows.Forms.GroupBox logGroupBox;
        private System.Windows.Forms.ProgressBar progressBar1;
        private System.Windows.Forms.RichTextBox LogBox;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TrackBar SpeedBar;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.GroupBox testGroupBox;
    }
}