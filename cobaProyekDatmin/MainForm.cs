﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;

namespace cobaProyekDatmin
{
    public partial class MainForm : Form
    {
        public MainForm()
        {
            InitializeComponent();
        }
        string filename = "";
        StreamReader reader;
        List<data> studentData = new List<data>();
        public static List<string[]> dataRows = new List<string[]>();
        public static Header[] headers;
        public static int headerBlacklistCount;

        public class Header
        {
            public string nama;
            public bool blacklist = false;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            OpenFileDialog o1 = new OpenFileDialog();
            o1.InitialDirectory = Application.StartupPath;
            o1.Title = "Load Dataset";
            o1.Filter = "TXT files|*.txt|CSV files|*.csv";
            if (o1.ShowDialog() == DialogResult.OK)
            {
                filename = o1.FileName.ToString();
                read(filename);
                btn_graph.Enabled = true;
                button2.Enabled = true;
            }
        }

        private void read(string file)
        {
            dataRows.Clear();
            reader = new StreamReader(file);
            string[] result = reader.ReadLine().Split(',');
            headers = new Header[result.Length];
            for (int i = 0; i < result.Length; i++)
            {
                headers[i] = new Header { nama=result[i] };
            }
            while (reader.Peek() > 0)
            {
                result = reader.ReadLine().Split(',');
                dataRows.Add(result);
            }
            reader.Close();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            if(headers == null)
            {
                MessageBox.Show("Load The Dataset First!!");
            }
            else
            {
                new ProcessForm().ShowDialog();
            }     
        }

        public static void blacklistHeader(int a)
        {
            headers[a].blacklist = true;
            headerBlacklistCount++;
        }

        public static void clearBlacklistHeader()
        {
            foreach (MainForm.Header header in MainForm.headers)
            {
                header.blacklist = false;
            }
            headerBlacklistCount = 0;
        }

        private void btn_graph_Click(object sender, EventArgs e)
        {
            new GraphicsForm().ShowDialog();
        }
    }
}
